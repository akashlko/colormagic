//
//  ColorTransferDelegate.swift
//  Color Magic
//
//  Created by Singh, Akash | RIEPL on 30/03/22.
//

import Foundation
import UIKit

protocol ColorTransferDelegate{
    func userDidChoose(color: UIColor, withName colorName:  String)
}
